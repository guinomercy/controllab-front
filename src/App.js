import React, { Component } from 'react';
import ReactTable from 'react-table';
import './App.css';
import "react-table/react-table.css";

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { tasks : [] }
  }

  componentDidMount() {
    const api = "https://jsonplaceholder.typicode.com/todos";
    fetch(api)
    .then(response => response.json())
    .then(todos => {this.setState({tasks : todos});})
    
  }
  render() {
    const header = [
      {Header: "UserId",
        accessor: "userId"},
        {Header: "Id",
        accessor: "id"},
        {Header: "Title",
        accessor: "title"}
    ]
    return (
      <ReactTable 
      columns={header}
      data={this.state.tasks}>
      </ReactTable>
    );
  }
}

export default App;